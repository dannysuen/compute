package com.example.android.mobileperf.compute;

import android.app.Application;

import com.frogermcs.androiddevmetrics.AndroidDevMetrics;

public class ComputeApplication extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        if (BuildConfig.DEBUG) {
            AndroidDevMetrics.initWith(this);
        }

        for (int i = 0; i < 50; i++) {
            Thread t = new Thread(new Runnable() {
                @Override
                public void run() {
                    Fibonacci.computeFibonacci(50);
                }
            });
            t.start();
        }
    }
}
