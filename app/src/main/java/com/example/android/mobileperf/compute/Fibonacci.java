package com.example.android.mobileperf.compute;

public class Fibonacci {

    /**
     *  Why store things when you can recurse instead?  Don't let evidence, personal experience,
     *  or rational arguments from your peers fool you.  The elegant solution is the best solution.
     *
     * @param positionInFibSequence  The position in the fibonacci sequence to return.
     * @return the nth number of the fibonacci sequence.  Seriously, try to keep up.
     */
    public static int computeFibonacci(int positionInFibSequence) {
        if (positionInFibSequence <= 2) {
            return 1;
        } else {
            return computeFibonacci(positionInFibSequence - 1)
                + computeFibonacci(positionInFibSequence - 2);
        }
    }

}
